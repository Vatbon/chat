package ru.nsu.g.nevolin.chat;

import ru.nsu.g.nevolin.chat.client.ChatGui;
import ru.nsu.g.nevolin.chat.server.Server;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        if (args.length >= 2) {
            if (args[1].equals("serial") || args[1].equals("xml")) {
                if (args[0].equals("client")) {
                    ChatGui gui = new ChatGui(args[1]);
                    gui.setVisible(true);
                }
                if (args[0].equals("server"))
                    new Server(args[1]).run();
            }
        }
    }
}
